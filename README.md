# Interactive Query Improvement Tool (IQIT)

(Note: In addition to these instructions, the following video explains how
to install and use the IQIT.)
https://www.dropbox.com/s/u0rr3ywcviebs5k/cs410-final-project.mp4?dl=0

This document is divided into the following sections:

* [Overview](#Overview of the function of the code)
* [Implementation detail](#How the software was implemented)
* [Installing and running the application](#Building the project)
* [Team member contributions](#Team member contributions)

## Overview of the function of the code
The Interactive Query Improvement Tool (IQIT) is a Java program that allows the user
to query Science Direct and improve their query using query expansion based on 
relevance feedback. The key use case is where a user is able to provide
relevance feedback on search results, and may be unfamiliar with the correct 
terminology to use for their query.

## How the software is implemented
This is a Java-based program whose main components are:
1. An interactive GUI
2. An API connection to Science Direct
3. A Rocchio feedback mechanism based on pseudo relevance and relevance feedback 
from the documents returned from Science Direct and relevance judgements supplied
by the user.

### Interactive GUI

The GUI is based on Java Swing and was implemented in IntelliJ IDEA Ultimate using
the UI Forms tool. 
The main class for the interactive tool is:
src/main/IQITMain

The main form is under:
src/main/IQITMain.form

The createUIComponents() method in IQITMain is responsible for custom initialization
and creation of GUI components that have some dependency that requires custom
creation.

The createAndShowGUI() method is what actually instantiates the main application
class and shows the interface to the user. 

There is a static main() method is IQITMain that will accept command line arguments
and invoke createAndShowGUI() 

builder. It is reommended to use 
 with sufficient detail so that others can have a basic understanding of your code for future extension or any further
  improvement. 
3) Documentation of the usage of the software including either documentation of usages of APIs or detailed instructions on how to install and run a software, whichever is applicable. 
4) Brief description of contribution of each team member in case of a multi-person team.

### Rocchio feedback mechanism

Rocchio feedback is used for both pseudo relevance and relevance feedback. The main
class for Rocchio feedback is: 
src/main/QueryExtender

To use the QueryExtender class you first instantiate it and then set the 
various parameters such as query, documents, alpha, beta, gamma, and top term 
counts.

For example: 

        QueryExtender queryExtender = new QueryExtender();
        queryExtender.setQuery(queryString);
        queryExtender.setAlpha(alphaFloat);
        queryExtender.setBeta(betaFloat);
        queryExtender.setGamma(gammaFloat);
        queryExtender.setTopTermsPerDocument(topTermCountInt);


This includes a relevance judgement per SearchResult instance, which is overridden
in the QueryExtender if isPseudo is true. If pseudo relevance is being used, then
you should pass the K parameter and set isPseudo to true:

        queryExtender.setPseudoK(pseudoKInt);
        queryExtender.setIsPseudo(isPseudo);

Once the parameters are set, the setDocuments() method is called with a list of
search results: List<SearchResult>

        queryExtender.setDocuments(feedbackDocuments);

Finally,to actually calculate Rocchio and get a list of terms, execute the
getRocchioFeedback() method:

        LinkedHashMap<String,Double> result = queryExtender.getRocchioFeedback();

Note that in the current version of the program, the gamma parameter for weighting
non-relevant documents is hard-coded.

#### How Rocchio was implemented with Lucene

An in-memory Lucene index is created (based on the RAMDirectory class for high
speed indexing and access). The index contains a document for every search 
result, and one document for the query itself. 

Each Lucene document representing a search result has four data fields:

* title
* keywords
* abstract
* combined

However, these field names are pre-pended with "irrelevant-" for documents that
have been scored as irrelevant. This allows for easy querying out of Lucene
for relevant vs irrelevant documents. 

The Lucene document representing a query has a single field:

* query

We create term vectors for **query**, **relevant documents**, and
**irrelevant documents** using the Lucene DirectoryReader class combined with the
LuceneDictionary class to iterate over terms in the **query**, 
**combined**, and **irrelevant-combined** fields, respectively.

        HashMap<String,Double> termVector = new HashMap<>();

        IndexReader reader = DirectoryReader.open(indexDirectory);

        LuceneDictionary ld = new LuceneDictionary(reader, field);

        BytesRefIterator iterator = ld.getEntryIterator();
        BytesRef byteRef = null;
        while ((byteRef = iterator.next()) != null) {
            String term = byteRef.utf8ToString();
            Term termInstance = new Term(field, term);
            termVector.put(term, (double)reader.totalTermFreq(termInstance));
        }

Once term vectors for query and relevant and irrelevant documents were created,
we looped through the terms in the union of these sets and for each term
calculated the new term weight based on the Rocchio formula:

    double termWeight = getAlpha() * queryTermWeight + (getBeta() * relevantTermWeight)
                        - (getGamma() * irrelevantTermWeight);
                        
We them sorted the result term vector and took the top N terms for the query 
expansion.

### API connection to Science Direct

Although the tool could be applied to other queryable data stores, we've implemented a connection to the ScienceDirect peer-reviewed journal, book chapter, and article database for the project's demonstration.

Our ScienceDirect connection implementation does its work primarily via two ScienceDirect public API webservice endpoints:

The endpoints both require an API key parameter.

The [first endpoint](https://dev.elsevier.com/documentation/SCIDIRSearchAPI.wadl#simple) performs a free-form text query, which returns an ordered set of document matches encoded as JSON along with some limited metadata information such as title and document id.

While this query does require an API key, it is availble for free to the general public and does not appear to need to originate from a subscriber network like UIUC's.

Queries to this endpoint originate from data.Connection.fetchJSON(String query).  From there, the endpoint query is dispatched to the more generic data.Connection.doGet(final String urlStr), which also takes care of issuing GETs for the second, document detail endpoint described below.

This query is performed via HTTP GET requests sent to [https://api.elsevier.com/content/search/sciencedirect](https://api.elsevier.com/content/search/sciencedirect)


THe [second endpoint](https://api.elsevier.com/content/abstract/pii) takes an API Key and a document ID as parameters and retrieves more extensive document metadata such as author-assigned keywords and full abstract text.

HTTP GET requests to this enpoint will yield a 401 error if they'd not contain a paid API key or originate from the network of a subscribing institution such as UIUC.  Because of this, it was necessary for us to be logged into the UIUC VPN with all network traffic tunneled through the VPN in order to successfully GET from this endpoint.

Queries to this second endpoint are executed in the data.Result.getAbstract() method (which fetches more than just the abstract, populating other document metadata with the rest of the response).

Requests to this second endpoint take an average of 3-4 seconds to fulfill.  Given that we're fetching 100 documents' details by default, a full fetch could take quite a while.  In practice we found that dispatching these GETs via a 15-thread threadpool yielded a full set of results in 10-15 seconds rather than waiting 5 minutes.

As a further optimization, we cache these GET results on disk by using the Apache [commons codec](https://commons.apache.org/proper/commons-codec/) sha1Hex implementation to generate a filename key by hashing the fetched URL, storing the resulting response in the file.  This drops the second endpoint query times from 3500ms to 2ms on cache hits.

For both endpoints, we use an org.json library to parse the endpoint response payload and traverse the results to extract portions of the payload that are most useful for our Rocchio processing.

## Building the project

#### required tools
To build the project from source, you will need to have javac 8+ installed and in your PATH:
```
cs410> java -version
java version "11.0.1" 2018-10-16 LTS
Java(TM) SE Runtime Environment 18.9 (build 11.0.1+13-LTS)
Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.1+13-LTS, mixed mode)
```

For command line builds (the simplest approach), you'll also need a recent release of [apache ant](https://ant.apache.org/) installed and in your PATH:
```
cs410> ant -version
Apache Ant(TM) version 1.10.5 compiled on July 10 2018
```

#### configure and build jar
Clone the git repository and cd into its root:
```
git clone https://gitlab.com/ae35unit/cs410.git && cd cs410

```
The ```build.properties``` is loaded by the ant build process and should contain a single entry: the location of JAVA_HOME.
We've included a simple python script that will attempt to generate a ```build.properties``` file based on your current environment:
```
:cs410> cat build.properties 
# run configure.py to generate this file
cs410> ./configure.py 
wrote /Users/gibbs/tmp/cs410/build.properties
cs410> cat build.properties 
jdk.home.1.8=/Library/Java/JavaVirtualMachines/jdk-11.0.1.jdk/Contents/Home 
```

With your JAVA_HOME correctly recorded in the build.properties file, simply run ```ant``` in the project's root directory to build our ```iqit.jar``` file:
```
cs410> ant
Buildfile: /Users/gibbs/tmp/cs410/build.xml
...
BUILD SUCCESSFUL
Total time: 2 seconds

cs410> ls -lh iqit.jar 
-rw-r--r--  1 ------  staff   5.7M Dec  9 17:39 iqit.jar
```

1. clone the git repository and cd into its root.
1. generate the build configuration by running configure.py
1. build the project by running ant with no arguments

The output of the build is the iqit.jar file.

#### log into the VPN if needed
Before starting the application, it's important that your machine appears to be on a UIUC network.  If you're not physically on a UIUC network, log into the VPN, being sure to select "tunnel all":
![](tunnelall.png)
The application will be querying some parts of the ScienceDirect API that require either payment or a subscription.  Being no a UIUC network gives you access to the UIUC subscription.

#### start the iqit application

With the iqit.jar now in place, starting the application will be a simple java command:
```
cs410> java -jar iqit.jar
```
The GUI should launch and look roughly like this:
![](freshlaunch.png)

## Team member contributions

Each of the two team members contributed equally to this project. 

* Kevin Mackie - kevindm2@illinois.edu
* Jackson Gibbs - jdgibbs2@illinois.edu

Work was distributed as follows:

* GUI - Kevin
* Science Direct APIs - Jackson
* Rocchio - Kevin
* Documentation of installation and build - Jackson
* Repository management - Jackson
* Project video - Kevin
* Installation and build automation - Kevin and Jackson
* Project documentation (this readme.md file) - Kevin and Jackson
* Tech review - Kevin and Jackson
