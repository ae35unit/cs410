#!/usr/bin/env python
import os
import shutil

def findhome():
    envvar = "JAVA_HOME"
    ret = None
    if envvar in os.environ:
        ret = os.environ[envvar]
    else:
        javac = shutil.which("javac")
        if javac is None:
            raise Exception("javac is not in your PATH")
        javabin = os.path.dirname(javac)
        ret = os.path.dirname(javabin)
    if ret is None:
        raise Exception("{} env var not found and no javac found in your PATH".format(envvar))
    return ret

outpath = os.path.abspath(("build.properties"))

with open(outpath, 'w') as f:
    f.write("{}={}\n".format("jdk.home.1.8", findhome()))
print("wrote {}".format(outpath))

