# Technology Review

Note that the source and latest copy of this technology review can be found in the 
following repository:
<https://gitlab.com/ae35unit/cs410/blob/master/doc/README.md>

## Overview
The objective of this technology review is to evaluate the required building blocks that we'd need to support our final project's goal of doing query expansion based on user feedback for queries to the [Science Direct](https://www.sciencedirect.com/) database.

We identify the criteria of our evaluation in the form of requirements we anticipated in the project and then evaluate potential tools based on those criteria.

Ultimately we found that Lucene was a good fit and Lemur fell short of some requirements and was wanting for useful documentation.

The final bit of our evaluation was our data source: the Science Direct APIs.

## Team Member Contributions

* [Lucene Evaluation](#lucene): Kevin Mackie (kevindm2@illinois.edu)
* [Lemur Evaluation](#lemur): Jackson Gibbs (jdgibbs2@illinois.edu)
* [Science Direct APIs](#science-direct-apis): Jackson
* [Other Query Expansion Resources](#Other Query Expansion Resources): Kevin

## Toolkit Evaluation Criteria

### Practical Toolkit Requirements

Our project will ultimately be a rich client Java GUI.  It will accept a query and use the ScienceDirect API to gather a set of candidate documents and their ScienceDirect metadata such as title, keywords, and abstract.  With this displayed, it will then gather and use user relevance judgements to suggest improved search terms.

The project will be implemented in Java, so a good Java API will be an important requriement for any fitting toolkit.

Being a rich client, the project will have a larger installation footprint than a browser-based solution.  We'd like to keep the installation process as simple and convenient as possible.  Ideally, this will be achieved by publishing the whole application in a single jar so that users can execute it with a simple ```java -jar app.jar``` command.

This installation footprint goal will mean that we're also looking for a toolkit that provides a self-contained Java API that doesn't require heavier deployment details like starting multiple processes e.g.: interdependent services.  We'd like all of the toolkit functionality to be available in the JRE process and loadable via the toolkit's jar.

### Functional Toolkit Requirements

At the outset, not having completed all of the course material yet, it is difficult to say exactly what functionality a fitting toolkit will need to provide us, but here are our anticipated needs:

- The toolkit should provide the ability to perform stemming
- The [Rocchio Algorithm](https://en.wikipedia.org/wiki/Rocchio_algorithm) appears often enough in readings regarding query expansion and relevance feedback that it seems any fitting solution should provide an implementation.
- A nice-to-have feature would be any API that directly addresses query expansion, such as an interface with multiple implementations that can be swapped out and reconfigured.

## Lucene

We describe here how to build an analyzer chain with Lucene 7.5.0 to perform 
tokenization, stop-word removal, stemming, indexing, synonym expansion, term retrieval. 
The motivation for this review was a project to add a Rocchio feedback loop to online 
queries of ScienceDirect. The information on the Lucene website was scattered and 
confusing, so we hope this review will help others more understand the concepts
and how to apply them.

### Concepts

The **Analyzer** class in Lucene is the key to creating an analysis chain. The class
does do work itself but creates instances of other classes to perform
tokenization and various analysis and filtering tasks. Analyzers can tie together 
instances of the following classes:
* Tokenizer - class responsible for converting text stream into a stream of tokens 
(words/terms)
* TokenFilter - modifies token stream e.g. lower-casing, stop word removal, stemming

### The basic default analyzer StandardAnalyzer
There is a basic Analyzer chain in Lucene called StandardAnalyzer() that performs
whitespace tokenization, lower casing, and stop word removal.

However, this basic analyzer was not sufficient for our purposes and so the rest 
of this tutorial is on how to build an analyzer and how to modify the behavior
of the tokenizers and filters for specific purposes.

### Required Lucene jars

Download Lucene from http://lucene.apache.org

For the code in this tutorial the only jars that need to be added to your Java project 
are: 
* lucene-analyzers-common-*.jar
* lucene-core-*.jar

### Create a sub-class of Analyzer

To create your own Analyzer you should extend the Analyzer class and implement
the createComponents() method.

The following Java source shows the creation of an analyzer chain implemented as a 
sub-class of Analyzer.

````
package com.kevinmackie;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

public class TechReviewAnalyzer extends Analyzer {

    private org.apache.lucene.util.Version matchVersion;

    public TechReviewAnalyzer(Version matchVersion) {
        this.matchVersion = matchVersion;
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        Tokenizer whitespaceTokenizer = new WhitespaceTokenizer();
        TokenStream lowerCaseFilter = new LowerCaseFilter(whitespaceTokenizer);
        TokenStream stopWordAnalyzer = new StopFilter(lowerCaseFilter, EnglishAnalyzer.ENGLISH_STOP_WORDS_SET);
        TokenStream snowballFilter = new PorterStemFilter(stopWordAnalyzer);

        return new TokenStreamComponents(whitespaceTokenizer,snowballFilter);
    }
}
````

The createComponents() method returns a TokenStreamComponents object
that contains the chain of tokenizers and filters through which our text data 
will pass for analysis.

### Parse text into individual tokens / terms

The first analysis step is to divide the text stream into discrete tokens or words.
This is achieved with a tokenizer. We use the standard WhitespaceTokenizer() for this:

````
Tokenizer whitespaceTokenizer = new WhitespaceTokenizer();
````

### Convert words to lowercase

We then convert the terms to lowercase so that we don't have to worry about matching
case when searching for terms. This is done by creating a new instance of the
LowerCaseFilter() class. Notice that our WhitespaceTokenizer instance is passed
as an argument to the constructor. This sets up a chain of analyzers.

````
TokenStream lowerCaseFilter = new LowerCaseFilter(whitespaceTokenizer);
````

### Eliminate stop words

Next we eliminate stop words such as "the", "a", "and". This is done by creating 
an instance of the StopFilter class. Again, notice how we pass the instance of
the previously created analyzer (i.e. lowerCaseFilter) as an argument to the 
constructor of the next analyzer in the chain. 

In this case we also pass in a list of stop words. We use the baseline English
stop words set. However, you can specify your own list of stop words here as well.

````
TokenStream stopWordFilter = new StopFilter(lowerCaseFilter, EnglishAnalyzer.ENGLISH_STOP_WORDS_SET);
````

### Apply stemming

Finally, we stem the terms so that inflected forms of the same term all result 
in the same token. For example, stemming will convert "run", "runs", "running" into
the root form "run" - so that the previous three terms will be counted as three
instances of the same term "run".

We use an instance of the PorterStemFilter class, and as usual pass the instance of
the previous analyzer class as an argument to the constructor to build the chain.

````
TokenStream porterFilter = new PorterStemFilter(stopWordAnalyzer);
````

Note that there are two stemmers that come native with Lucene. The other is the 
"Snowball" stemmer, which can be created in similar fashion.

````
TokenStream snowballFilter = new SnowballFilter(stopWordAnalyzer);
````

### Return the final analyzer instance from createComponents()

Finally, we return the last TokenStream instance i.e. porterFilter from the 
createComponents() method. 

### Implement Rocchio using Lucene

Lucene contains basic indexing functions that are useful for implementing
Rocchio feedback:

* In-memory Lucene index based on RAMDirectory class can store search results
and query so that we have an easy way to collect terms and their frequency
* Lucene DirectoryReader class combined with LuceneDictionary class can be used
to iterate over the terms to get term frequencies for Rocchio feedback
* Lucene supports fields in documents that are indexed separately - which can be
used to quickly query over sets of documents by field name - for example one 
representing queries, one representing relevant documents, one representing 
irrelevant documents
* Fields could also be used to support differential weighting of different fields

## Other Toolkits

Ultimately, we decided that Lucene best fit our project's needs.  This section summarizes the investigation and findings regarding other potential toolkits that led us to choose Lucene for our work.


### lemur

#### Overview

The [project itself](https://www.lemurproject.org/) is broken into many components, here's a partial list:
- Two search engines
  - Indri
  - Galago
- The Lemur Toolkit
  - The final released version of the Lemur Toolkit is version 4.12,released 06/21/2010
- Two Datasets (ClueWeb09, ClueWeb12)
- A ranking algorithm library

Compared to Apache projects like Lucene, the Lemur project's organization and even source control is pretty fragmented (split between a handful of old subversion and mercurial repositories).

Where it exists, Documentation is generally pretty sparse and seems to primarily be aimed at users wanting to deploy one of the two search engines as stand-alone applications.

As you can see from the component listing, "Lemur" appears to actually be a grab bag of subprojects.  Our challenge, then, is to investigate which of the subprojects might satisfy our project's requirements.

The Lemur toolkit seems like the part we'd be most interested in, but the official site indicates that it's no longer in active development and hasn't seen any maintenance in over eight years "```The final released version of the Lemur Toolkit is version 4.12, released 06/21/2010```".

This [paper](https://link.springer.com/content/pdf/10.1007%2F978-3-642-28765-7_48.pdf) is the most on-point discussion of query expansion I've found with respect to the Lemur projects.  It makes several references to query expansion and Rocchio in the context of Lemur, but is disappointingly light on Lemur-specific detail.


#### Lemur Practical Requirements Evaluation
Lemur's Java interface is implemented by using [JNI](https://en.wikipedia.org/wiki/Java_Native_Interface) to call into Lemur's C/C++ native libraries.

So, while Lemur does have a Java interface, using it would have required users (graders) to build platform-specfic native libraries rather than simply running entirely from Java code distributed as a jar with our project.

In short, Lemur won't satisfy our goal of all functionality fitting into a single jar in the form of cross-platform Java bytecode.  Using Lemur would have meant imposing a more complex build and install procedure (and related application documentation).

While there are binary downloads availale for some platforms, I've had issues building from source on my development machines.  This makes a pure Java cross platform solution look all the more attractive and gives me concern that we'd potentially have trouble getting everything built and working for users/graders when the time comes.


#### Lemur Functional Requirements Evaluation

##### Rocchio

I've found multiple papers that make reference to the Lemur toolkit implementing a simplified Rocchio feedback algorithm.  If that functionality actually exists in the toolkit, it appears to be entirely undocumented.  Even after pulling all available source I see no reference to rocchio anywhere.

Given that "Lemur Toolkit" (not to be confused with the umbrella "Lemur" project) was end-of-lifed more than eight years ago, the fact that persistent searching for documentation has yielded no help on this front seems like a red flag pointing us in the direction of other tools that are in more active use and have more fundamentally organized documenrtation.


##### Stemming

The Lemur Toolkit does provide this basic functionality in the form of two stemmers: Krovtz and Porter.

#### Lemur Conclusion

Lemur appears to be a haphazardly organized grab bag of IR work that's potentially suitable for someone who needs a full out-of-the-box search engine installation.

Somewhere in that grab-bag, it appears Lemur does implement the functionality we're interested in.  In some cases, however, finding that functionality anywhere in code was nearly impossible (even after pulling all of the project's various source repositories).

Where there is a Java interface, it is generally a thin wrapper providing access to native code implementations via JNI.  With no pure Java library, only sparse platform coverage in available pre-built binaries, and mixed results in trials of building from source, it seems unreasonable to expect Lemur to meet our needs if other more java-centric libraries are available.


### [sphinx](http://sphinxsearch.com/docs/sphinx3.html)

would have been next investigation topic given more time, Lucene had already emerged as a clear winner by this point.

### [xapian](https://xapian.org/docs/overview.html)

again, would have been next investigation topic given more time, Lucene had already emerged as a clear winner by this point.

## Science Direct APIs

The final piece of our project's puzzle would be accessing the ScienceDirect query interface and results from queries.

To do this, we'll need to execute a user query on the ScienceDirect site.  The user will be presented with the query results.  As the user provides feedback by selecting the most appropriate results, we'll use the selected documents' metadata such as abstracts, keywords, and titles as a body of documents from which to suggest expanded query terms.

At a minimum, then, we'd need to be able to perform online queries of the ScienceDirect site and fetch full metadata for each result we'll present to users.

We considered scraping the ScienceDirect site's web interface for this, but found that they publish a [web service API](https://dev.elsevier.com/documentation/SCIDIRSearchAPI.wadl) that's perfectly suited to this sort of work.

With an account and API key, users can access the general-purpose frontend query API via ```https://dev.elsevier.com/documentation/SCIDIRSearchAPI.wadl#d1e166```.

Fetching full result abstracts take a bit longer and requires users connect directly from UIUC networks (using a VPN connection if not physically on campus).  This is accessible via ```https://dev.elsevier.com/documentation/AbstractRetrievalAPI.wadl#d1e894```

These APIs accept and return JSON and are easily accessible and parsable with standard Java libraries.

## Other Query Expansion Resources

Queries can be expanded by a) analyzing the local text in the query and search 
result documents, and/or b) using external resources as a source for additional terms.

External resources include:

* Synonyms and acronyms - additional terms can be added to the index for 
synonyms and acronyms (e.g. ICS = "industrial control system")
* Spell checking and normalization - alternate or corrected spellings
can be used for some terms, and normalization can be performed to (say) convert
international characters such as á ñ to "a" and "n"

Lucene provides support for adding synonyms to query terms via a TokenFilter
instance in the analysis chain, as described above. Note that part of tokenization
 involves tracking of token position and length, which allows for (among other things)
proximity searches. When injecting synonyms into a Lucene index, it is important
to use the same token position for the synonym as the original term, but with a 
length of zero. This allows code to easily identify synonyms, since they will have
an identical position as the original term, but with length zero.

The actual source of synonyms (except in the case of syntagmatic context mining, 
say), could come from thesauri or other sources, such as Wikipedia articles
discovered using the original term and then analyzed for related terminology.
