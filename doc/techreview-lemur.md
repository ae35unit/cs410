### scratch area for working on lemur writeup

# misc notes for project writreup
The [paper on using Lemur in biomedical query expansion](https://link.springer.com/content/pdf/10.1007%2F978-3-642-28765-7_48.pdf) made reference to using [custom acronym lists](https://www.acronymslist.com/cat/human-genome-acronyms-p3.html) in stemming and stopword processing.  That might be a potential entry for an ```enhancements``` or ```ideas for future work``` section (the ability to choose domain-specific stopword lists or something).


# Other Toolkits

Ultimately, we decided that Lucene best fit our project's needs.  This section summarizes the investigation and findings regarding other potential toolkits that led us to choose Lucene for our work.

##### Practical Toolkit Requirements

Our project will ultimately be a rich client Java GUI.  It will accept a query and use the ScienceDirect API to gather a set of candidate documents and their ScienceDirect metadata such as title, keywords, and abstract.  With this displayed, it will then gather and use user relevance judgements to suggest improved search terms.

The project will be implemented in Java, so a good Java API will be an important requriement for any fitting toolkit.

Being a rich client, the project will have a larger installation footprint than a browser-based solution.  We'd like to keep the installation process as simple and convenient as possible.  Ideally, this will be achieved by publishing the whole application in a single jar so that users can execute it with a simple ```java -jar app.jar``` command.

This installation footprint goal will mean that we're also looking for a toolkit that provides a self-contained Java API that doesn't require heavier deployment details like starting multiple processes e.g.: interdependent services.  We'd like all of the toolkit functionality to be available in the JRE process and loadable via the toolkit's jar.

##### Functional Toolkit Requirements

At the outset, not having completed all of the course material yet, it is difficult to say exactly what functionality a fitting toolkit will need to provide us, but here are our anticipated needs:

- The toolkit should provide the ability to perform stemming
- The [Rocchio Algorithm](https://en.wikipedia.org/wiki/Rocchio_algorithm) appears often enough in readings regarding query expansion and relevance feedback that it seems any fitting solution should provide an implementation.
- A nice-to-have feature would be any API that directly addresses query expansion, such as an interface with multiple implementations that can be swapped out and reconfigured.


## [lemur](https://www.lemurproject.org/)

##### Overview

The project itself is broken into many components, here's a partial list:
- Two search engines
  - Indri
  - Galago
- The Lemur Toolkit
  - The final released version of the Lemur Toolkit is version 4.12,released 06/21/2010
- Two Datasets (ClueWeb09, ClueWeb12)
- A ranking algorithm library

Compared to Apache projects like Lucene, the Lemur project's organization and even source control is pretty fragmented (split between a handful of old subversion and mercurial repositories).

Where it exists, Documentation is generally pretty sparse and seems to primarily be aimed at users wanting to deploy one of the two search engines as stand-alone applications.

As you can see from the component listing, "Lemur" appears to actually be a grab bag of subprojects.  Our challenge, then, is to investigate which of the subprojects might satisfy our project's requirements.

The Lemur toolkit seems like the part we'd be most interested in, but the official site indicates that it's no longer in active development and hasn't seen any maintenance in over eight years "```The final released version of the Lemur Toolkit is version 4.12, released 06/21/2010```".



This [paper](https://link.springer.com/content/pdf/10.1007%2F978-3-642-28765-7_48.pdf) is the most on-point discussion of query expansion I've found with respect to the Lemur projects.


##### Practical Requirements Evaluation
Lemur's Java interface is implemented by using [JNI](https://en.wikipedia.org/wiki/Java_Native_Interface) to call into Lemur's C/C++ native libraries.

So, while Lemur does have a Java interface, using it would have required users (graders) to build platform-specfic native libraries rather than simply running entirely from Java code distributed as a jar with our project.

In short, Lemur won't satisfy our goal of all functionality fitting into a single jar in the form of cross-platform Java bytecode.  Using Lemur would have meant imposing a more complex build and install procedure (and related application documentation).

While there are binary downloads availale for some platforms, I've had issues building from source on my development machines.  This makes a pure Java cross platform solution look all the more attractive and gives me concern that we'd potentially have trouble getting everything built and working for users/graders when the time comes.


#### Functional Requirements Evaluation

##### Rocchio

I've found multiple papers that make reference to the Lemur toolkit implementing a simplified Rocchio feedback algorithm.  If that functionality actually exists in the toolkit, it appears to be entirely undocumented.  Even after pulling all available source I see no reference to rocchio anywhere.

Given that "Lemur Toolkit" (not to be confused with the umbrella "Lemur" project) was end-of-lifed more than eight years ago, the fact that persistent searching for documentation has yielded no help on this front seems like a red flag pointing us in the direction of other tools that are in more active use and have more fundamentally organized documenrtation.


##### Stemming

The Lemur Toolkit does provide this basic functionality in the form of two stemmers: Krovtz and Porter.

##### Conclusion

Lemur appears to be a haphazardly organized grab bag of IR work that's potentially suitable for someone who needs a full out-of-the-box search engine installation.

Somewhere in that grab-bag it does implement the functionality we're interested in.  In some cases, however, finding that functionality anywhere in code was nearly impossible (even after pulling all of the project's various source repositories).

Where there is a Java interface, it is generally a thin wrapper providing access to native code implementations via JNI.  With no pure Java library, only sparse platform coverage in available pre-built binaries, and mixed results in trials of building from source, it seems unreasonable to expect Lemur to meet our needs if other more java-centric libraries are available.


## [sphinx](http://sphinxsearch.com/docs/sphinx3.html)

would have been next investigation topic given more time

## [xapian](https://xapian.org/docs/overview.html)

would have been next investigation topic given more time




