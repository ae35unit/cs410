<meta charset="utf-8"/>
<co-content>
 <h3 level="3">
  <strong>
   Introduction
  </strong>
 </h3>
 <p>
  The course project is to give the students hands-on experience on developing some novel information retrieval and/or text mining tools. The project thus emphasizes "deliverables," and the outcome of your project should be an open-source contribution of a software tool or a component of a software tool that other people can download and use. Since the project lasts for the entire semester, it would allow the students to potentially apply all the knowledge and skills learned in the course to solve a real-world problem. Group work is encouraged, but not required (i.e., you can have a one-person team). The maximum size of a team is 3 members to avoid challenges in efficient coordination of the work by too many team members. However, a team of a larger size is also possible subject to the approval of the instructor. A typical reason for a larger team is because the project has a very natural task division among the team members so that the need for frequent interactions and coordination of team members may be minimum despite the large size of the team. Whenever possible, collaboration of multiple project teams is strongly encouraged to minimize the amount of work of each team via expertise or resource sharing, as well as to generate “combined impact” (e.g., one team may develop a crawler that can be used by another team that develops a search engine).
 </p>
 <h3 level="3">
  <strong>
   Grading criteria
  </strong>
 </h3>
 <p>
  Your project will be graded based on the following weighting scheme, corresponding to three stages of work including 1) topic selection; 2) proposal development; 3) progress report; 4) project result submission. All team members will receive the same grade provided that every member has made sufficient effort (at least 20 hours of quality time to work on the project).
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    <strong>
     Project topic selection (5%):
    </strong>
    Every student is required to select a project topic from a list of given topics or proposal a topic around week 4. Project topic selection will be graded based on completion. The topic selection would facilitate team formation.
   </p>
  </li>
  <li>
   <p>
    <strong>
     Project proposal (5%):
    </strong>
    Each project team is required to submit a one-page project proposal around week 6, which will be graded based on completion. The instructor and TAs will read every proposal to provide feedback and suggestions so as to ensure every team to have an interesting, yet tractable project that can be realistically finished by the end of the semester. Relevant research papers or readings may be recommended.
   </p>
  </li>
  <li>
   <p>
    <strong>
     Project progress report (5%)
    </strong>
    : Around week 13, each project team will be asked to submit a very brief progress report with three short paragraphs to cover, respectively, 1) what has been done, 2) what remains to be done, and 3) any barrier(s) in achieving the proposed project goal. Progress report would be graded based on completion, and should be
    <strong>
     submitted on Piazza
    </strong>
    .
   </p>
  </li>
  <li>
   <p>
    <strong>
     Software code submission with documentation (65%):
    </strong>
    At the end of the semester, each project team will be asked to submit the produced source code with reasonable documentation to Github. The documentation should cover both how to use the software and how the software is implemented. The 65% of the grade would be distributed as follows:
    <strong>
     45% for source code submission
    </strong>
    ;
    <strong>
     20% for documentation submission
    </strong>
    . Both would be graded based on completion, and should be
    <strong>
     uploaded to Coursera
    </strong>
    .
   </p>
  </li>
  <li>
   <p>
    <strong>
     <a href="http://sifaka.cs.uiuc.edu/course/410s15/project.html#present">
      Software usage tutorial presentation
     </a>
    </strong>
    <strong>
     (20%):
    </strong>
    At the end of the semester, every project team will be asked to submit a short tutorial presentation (e.g., a voiced ppt presentation) to explain how the developed software is to be used. The presentation must include (1) sufficient instructions on how to install the software if applicable, (2) sufficient instructions on how to use the software, and (3) at least one example of use case so as to allow a grader (instructor, TA, or a project supervisor) to use the provided use case to test the software. The tutorial presentation would be graded based on 1) completion of the presentation (10%); and 2) result of testing the software by graders (10%). If the software passes the test, i.e., is working as expected, full points will be given; otherwise, points will be deducted from the 10% allocated to the “result of testing the software by graders.” This should be
    <strong>
     uploaded to Coursera
    </strong>
    .
   </p>
  </li>
 </ul>
 <p>
  Thus your course project work would be graded primarily based on your effort. If you have followed our guidelines and completed all the required tasks, you should receive at least 90% of the total points for the project. This is to encourage the students to pay attention to time management and set realistic goals that can actually be completed by the end of the semester. The remaining 10% is based on how well your software works; a fully functioning software would be given the whole 10%, whereas a buggy software or a software with missing functions would result in losing some of the 10% of the grade. Completion of a functioning software is emphasized also due to the potential dependency between multiple projects when they are all contributing to a larger project (e.g., one team may produce a crawler to crawl data for use by another team to build a search engine).
 </p>
 <h3 level="3">
  <strong>
   Instructions
  </strong>
 </h3>
 <p>
  <strong>
   1. Pick a topic (due end of Week 4)
  </strong>
 </p>
 <p>
  Around Week 4, every student will be asked to pick a project topic. You can either pick from a list of sample topics provided by the instructor and TAs (will be posted on Piazza before Week 4) or propose your own topic. We encourage those of you who have identified a practical challenge related to your work/job to design a course project to solve the identified challenge. This would maximize the utility of your project since the outcome may be something useful for your job. However, please make sure to inform your company and obtain an approval since any software code you produced for the course project must be available to the public.
 </p>
 <p>
  In the case of proposing a topic, you should post a brief description of the topic (at least two short paragraphs) on Piazza using the tag/folder “projtopic”; please make sure to choose the folder “projtopic” to ensure visibility of your proposed topic. The provided topics by the instructor and TAs are also tagged with this same folder, so one can easily view all the potential project topics by using the folder “projtopic”. We encourage you to look at the ideas posted there, discuss them, and post your comments.
 </p>
 <p>
  When proposing a topic, try to ask yourself the following questions:
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    What is exactly the function of the tool that you would like to develop?
   </p>
  </li>
  <li>
   <p>
    Is the envisioned tool really useful? Who will benefit from such a tool? Note that the usefulness of the tool is an important grading factor, so make sure that you invest your effort on developing a really useful tool. It's fine if the tool is a very specialized tool, or only useful for a few people.
   </p>
  </li>
  <li>
   <p>
    Does this kind of tools already exist? If similar tools exist, how is your tool different from them? Would people care about the difference? Why is this difference important?
   </p>
  </li>
  <li>
   <p>
    Do you have some rough idea about how the target function might be achieved? (The instructor and TAs can help you answer this question, so please feel free to propose problems that you don’t yet have a clear idea about how to solve it)
   </p>
  </li>
  <li>
   <p>
    How do you plan to show people that the tool to be developed is indeed useful? (You could make a demo of its function and/or report some quantitative evaluation figures.)
   </p>
  </li>
  <li>
   <p>
    What is the minimum goal to be achieved during this semester? (Try to drop everything non-essential and only keep the function that is truly novel.)
   </p>
  </li>
 </ul>
 <p>
  <strong>
   2. Form a team
  </strong>
 </p>
 <p>
  You are encouraged to work with other students in a team, but it is fine if you decide to work on your own (i.e., a one-person team). Teamwork not only allows you to learn from each other, but also enables you to accomplish more through the project work than you could have with just yourself (e.g. finishing a more sophisticated system). However, working in a team also require significant communications and coordination with team members; for this reason, the size of a team should generally be no more than three members. Teams with more than three members must be approved by the instructor or a TA (the approval will be primarily based on whether there is a clear division of work among all the team members so as to avoid complexity in communication and coordination). Generally speaking, all the team members of a group will get the same grade provided all have contributed
  <em>
   substantially
  </em>
  to the project. In case there is evidence that a team member has only made superficial contribution to a project (we really hope this won't happen!), the particular team member's grade may be discounted. The documentation of the project (to be submitted at the end of the semester) must state clearly who did what.
 </p>
 <p>
  <strong>
   3. Write a proposal (due end of Week 6)
  </strong>
 </p>
 <p>
  Each project team is required to write a one-page proposal before you actually go in depth on a topic and post your proposal on Piazza using the tag/folder “projproposal”. The proposal is due around Week 6.
 </p>
 <p>
  In the proposal, you should address the following questions and include the names and email addresses of all the team members. One member must be designated as the project coordinator for the team, so please make sure to indicate that. The project coordinator would be responsible for the coordination of the project work by the team and also communication with the instructor or TA when the team needs help. (As long as these questions are addressed, the proposal does not have to be very long. A couple of sentences for each question would be sufficient.)
 </p>
 <ul bullettype="bullets">
  <li>
   <p>
    What is the function of the tool?
   </p>
  </li>
  <li>
   <p>
    Who will benefit from such a tool?
   </p>
  </li>
  <li>
   <p>
    Does this kind of tools already exist? If similar tools exist, how is your tool different from them? Would people care about the difference?
   </p>
  </li>
  <li>
   <p>
    What existing resources can you use?
   </p>
  </li>
  <li>
   <p>
    What techniques/algorithms will you use to develop the tool? (It's fine if you just mention some vague idea.)
   </p>
  </li>
  <li>
   <p>
    How will you demonstrate the usefulness of your tool.
   </p>
  </li>
  <li>
   <p>
    A very rough timeline to show when you expect to finish what. (The timeline doesn't have to be accurate.)
   </p>
  </li>
 </ul>
 <p>
  If you have selected a provided project topic, the answers to some of the questions should have already been available to you (via the provided project description); if you proposed your own topic, please make sure that there is clearly a use case of your proposed tool. At the proposal stage, the two most important criteria to keep in mind are novelty and potential impact; an ideal project proposal should contain a clearly novel idea which would lead to a clearly useful tool for at least one real user (if the project is successful).
 </p>
 <p>
  <strong>
   4. Work on the project
  </strong>
 </p>
 <p>
  You should try to reuse any existing tools as much as possible so as to minimize the amount of work without sacrificing your goal. Discuss any problems or issues with your teammates or classmates on Piazza and leverage Piazza to collaborate with each other. Consistent with our course policy, we strongly encourage you to help each other in all the course work so as to maximize your gain of new knowledge and skills while minimizing your work as much as possible. We will do our best to help you as well. Consider documenting your work regularly. This way, you will already have a lot of things written down by the end of the semester.
 </p>
 <p>
  <strong>
   5.
  </strong>
  <strong>
   Progress report (5%, due end of Week 13)
  </strong>
 </p>
 <p>
  Every team is required to submit a brief progress report around Week 13, which should contain at least three short paragraphs to cover, respectively, 1) what has been done, 2) what remains to be done, and 3) any barrier(s) in achieving the proposed project goal. Progress report would be graded based on completion. It is especially important that you report any problem or difficulty you have encountered so that the instructor and TAs can help you solve the problem or otherwise adjust your project goal in a timely manner. The progress report should be posted as a comment/reply to your own project proposal.
 </p>
 <p>
  <strong>
   6.
  </strong>
  <strong>
   Software code submission with documentation (65%, due the last day of the final examination period)
  </strong>
 </p>
 <p>
  At the end of the semester, each team must submit the software code produced for the project to an open source repository (github) along with a written documentation. The documentation should consist of the following elements: 1) An overview of the function of the code (i.e., what it does and what it can be used for). 2) Documentation of how the software is implemented with sufficient detail so that others can have a basic understanding of your code for future extension or any further improvement. 3) Documentation of the usage of the software including either documentation of usages of APIs or detailed instructions on how to install and run a software, whichever is applicable. 4) Brief description of contribution of each team member in case of a multi-person team. Note that
  <strong>
   if you are in a team, it is your responsibility to figure out how to contribute to your group project, so you will need to act proactively and in a timely manner if your group coordinator has not assigned a task to you.
  </strong>
  There will be no opportunity to make up for any task that you failed to accomplish.
  <strong>
   In general, all the members of a team will get the same grade for the project unless the documentation submission indicates that some member(s) only superficially participated in the project without doing much actual work; in that case, we will discount the grade.
  </strong>
  Everyone is expected to spend at least 20 hours to seriously work on your course project as a minimum, not including the time spent for preparing the documentation.
 </p>
 <p>
  The 65% of the grade would be distributed as follows:
  <strong>
   45% for source code submission
  </strong>
  ;
  <strong>
   20% for documentation submission.
  </strong>
  The 20% for the documentation submission includes 5% for overview of functions, 10% for implementation documentation, 5% for usage documentation. There is no strict length requirement for the documentation.
 </p>
 <p>
  <strong>
   7.
  </strong>
  <strong>
   Software usage tutorial presentation (20%, due the last day of the final examination period)
  </strong>
 </p>
 <p>
  At the end of the semester, every project team will be asked to submit a short tutorial presentation (e.g., a voiced ppt presentation) to explain how the developed software is to be used. The presentation must include (1) sufficient instructions on how to install the software if applicable, (2) sufficient instructions on how to use the software, and (3) at least one example of use case so as to allow a grader (instructor, TA, or a project supervisor) to use the provided use case to test the software. There is no strict length requirement for this video submission, but you should target at 5~10 minutes. A presentation shorter than 5 minutes is unlikely detailed enough to help users understand how to use the software, whereas a longer video than 10 minutes might be too long for impatient users. However, feel free to produce a longer presentation if needed.
 </p>
 <p>
  We (instructor, TAs, or project supervisors) will watch all your tutorial presentations and attempt to test each software to check whether it works as described in the video presentation. The tutorial presentation would be graded based on
 </p>
 <p>
  1) completion of the presentation (10%); and
 </p>
 <p>
  2) result of testing the software by graders (10%).
 </p>
 <p>
  If the software passes the test (i.e., is working as expected), full points will be given; otherwise, points will be deducted from the 10% allocated to the “result of testing the software by graders.”
 </p>
 <p>
 </p>
 <p>
 </p>
 <p>
 </p>
</co-content>
<style>
 body {
    padding: 50px 85px 50px 85px;
}

table th, table td {
    border: 1px solid #e0e0e0;
    padding: 5px 20px;
    text-align: left;
}
input {
    margin: 10px;
}
}
th {
    font-weight: bold;
}
td, th {
    display: table-cell;
    vertical-align: inherit;
}
img {
    height: auto;
    max-width: 100%;
}
pre {
    display: block;
    margin: 20px;
    background: #424242;
    color: #fff;
    font-size: 13px;
    white-space: pre-wrap;
    padding: 9.5px;
    margin: 0 0 10px;
    border: 1px solid #ccc;
}
</style>
<script async="" src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript">
</script>
<script type="text/x-mathjax-config">
 MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [ ['$$','$$'], ['$','$'] ],
      displayMath: [ ["\\[","\\]"] ],
      processEscapes: true
    }
  });
</script>
