package main;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.spell.LuceneDictionary;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.BytesRefIterator;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class handles extending queries based on a set of search results.
 * It is instantiated with a query term and then subsequently has methods to return extended queries based
 * on relevance feedback (sets of documents with indication of relevance/irrelevance)
 *
 */
public class QueryExtender {

    private boolean isPseudo;
    private Integer pseudoK;
    private int relevantCount;
    private int irrelevantCount;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getTopTermsPerDocument() {
        return topTermsPerDocument;
    }

    public void setTopTermsPerDocument(int topTermsPerDocument) {
        this.topTermsPerDocument = topTermsPerDocument;
    }

    public double getAlpha() {
        return alpha;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    // In this version the gamma parameter for weighting the irrelevant document terms is fixed in the constructor.
    // a future improvement would be to make this configurable
    private double getGamma() {
        return gamma;
    }
    public void setGamma(float gamma) {
        this.gamma = gamma;
    }

    public ArrayList<SearchResult> getDocuments() {
        return documents;
    }

    public void setDocuments(List<SearchResult> documents) {
        for (SearchResult searchResult : documents) {
            addDocument(searchResult);
        }
    }

    private String query;
    private int topTermsPerDocument;
    private double alpha;
    private double beta;
    private double gamma; // Note that the gamma parameter, which is the weight for irrelevant documents, is fixed (see constructor)
    private ArrayList<SearchResult> documents;
    private RAMDirectory indexDirectory;
    private Analyzer analyzer;
    private IndexWriter writer;

    QueryExtender() {
        isPseudo = false;
        setQuery("");
        documents = new ArrayList<>();

        // Initialize this query extender with some default values for the Rocchio calculation
        setTopTermsPerDocument(10); // Number of top terms to consider per document
        setAlpha(0.5f); // Alpha is the weight for the original query terms
        setBeta(0.5f); // Beta is the weight for the extended query terms
        setGamma(0.2f);
        indexDirectory = null;
        analyzer = null;
        writer = null;
        relevantCount = 0;
        irrelevantCount = 0;
    }

    /**
     * This private HashMap class allows for comparison and sorting of search results based
     * on weight.
     *
     * @param <K> = key type
     * @param <V> = value type
     */
    private class MyHashMap<K,V> extends HashMap<K,V> implements Comparator<Map.Entry<String,Double>> {

        @Override
        public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
            return Double.compare(o1.getValue(),o2.getValue());
        }

        @Override
        public Comparator<Entry<String, Double>> reversed() {
            return null;
        }

    }
    /**
     * Given the set of top n terms for each relevant document (however relevance was determined)
     * Calculate the Rocchio feedback as ordered list of terms whose weights are based on Rocchio i.e.
     * * Weight (i.e. count) of term in query
     * * Average weight (i.e. count) of term in set of terms
     * @return
     */
    public LinkedHashMap<String,Double> getRocchioFeedback() {

        if (documents.size() == 0) return null;

        HashMap<String,Double> unsortedExpandedQueryVector = new HashMap<>();

        try {
            indexQuery();
            commitPendingIndexWrites();

            HashMap<String,Double> queryTermVector = getTermVector("query");
            HashMap<String,Double> relevantTermVector = getTermVector("combined");
            HashMap<String,Double> irrelevantTermVector = getTermVector("irrelevant-combined");

            HashSet<String> terms = new HashSet<>();
            terms.addAll(relevantTermVector.keySet());
            terms.addAll(irrelevantTermVector.keySet());
            terms.addAll(queryTermVector.keySet());

            for (String term : terms) {
                double queryTermWeight = 0.0;
                double relevantTermWeight = 0.0;
                double irrelevantTermWeight = 0.0;

                if (queryTermVector.containsKey(term)) {
                    queryTermWeight = queryTermVector.get(term);
                }
                if (relevantTermVector.containsKey(term)) {
                    relevantTermWeight = relevantTermVector.get(term)/(double)relevantCount;
                }
                if (irrelevantTermVector.containsKey(term)) {
                    irrelevantTermWeight = irrelevantTermVector.get(term)/(double)irrelevantCount;
                }

                double termWeight = getAlpha() * queryTermWeight + (getBeta() * relevantTermWeight)
                        - (getGamma() * irrelevantTermWeight);

                unsortedExpandedQueryVector.put(term,termWeight);
            }



        } catch (IOException e) {
            e.printStackTrace();
        }
        return unsortedExpandedQueryVector.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (a, b) -> a,
                        LinkedHashMap::new
                ));
    }

    /**
     * Given an index field return a list of terms of their weights.
     *
     * @param field the field to query for the terms. Will be one of: relevant-combined, irrelevant-combined, query
     * @return returns a map of terms and their weights (the count of terms in the index
     * @throws IOException
     */
    private HashMap<String,Double> getTermVector(String field) throws IOException {
        HashMap<String,Double> termVector = new HashMap<>();

        IndexReader reader = DirectoryReader.open(indexDirectory);

        LuceneDictionary ld = new LuceneDictionary(reader, field);

        BytesRefIterator iterator = ld.getEntryIterator();
        BytesRef byteRef = null;
        while ((byteRef = iterator.next()) != null) {
            String term = byteRef.utf8ToString();
            Term termInstance = new Term(field, term);
            termVector.put(term, (double)reader.totalTermFreq(termInstance));
        }
        return termVector;
    }


    /**
     * Add the query terms to our document index. This adds the terms to the Lucene
     * inverted index and is used to loop through all of the query and document terms
     * to apply the Rocchio algorithm.
     */
    private void indexQuery() {

        checkAndInitializeIndex();

        Document queryDoc = new Document();
        queryDoc.add(new StringField("type", "query", Field.Store.YES));
        queryDoc.add(new TextField("query", getQuery(), Field.Store.YES));

        try {
            writer.addDocument(queryDoc);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Commits any pending writes to the index so that it is up-to-date and can be used to extract terms and frequencies
     */
    private void commitPendingIndexWrites() {
        if (writer != null) {
            try {
                writer.commit();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Search results are added to the QueryExtender by calling this method.
     * Each document results in a Lucene document being added to the index.
     * The document contains the following fields:
     * 1. type i.e. whether this is document or query
     * 2. rank i.e. the numerical rank of the document in the index
     * 3. *-title
     * 4. *-keywords
     * 5. *-abstract
     * 6. *-combined
     *
     * Where *- is either relevant- or irrelevant-
     * and combined indicates a composite field with title + keyword + abstract
     *
     * The relevant/irrelevant- prefix is used so that later we can easily query
     * from the index all relevant docs or irrelevant docs.
     *
     * @param document the document to add to the index
     */
    public void addDocument(SearchResult document) {
        if (documents == null)
            documents = new ArrayList<>();

        documents.add(document);

        checkAndInitializeIndex();

        String fieldPrefix = "irrelevant-";
        if (isRelevant(document)) {
            relevantCount++;
            fieldPrefix = "";
        } else {
            irrelevantCount++;
        }

        Document queryDoc = new Document();
        queryDoc.add(new StringField("type", "document", Field.Store.YES));
        queryDoc.add(new StringField("rank", String.valueOf(getDocuments().size()), Field.Store.YES));
        queryDoc.add(new TextField(fieldPrefix + "title", document.getTitle(), Field.Store.YES));
        queryDoc.add(new TextField(fieldPrefix + "keywords", document.getKeywords(), Field.Store.YES));
        queryDoc.add(new TextField(fieldPrefix + "abstract", document.getAbstractText(), Field.Store.YES));
        queryDoc.add(new TextField(fieldPrefix + "combined", document.getCombinedFields(), Field.Store.YES));

        try {
            writer.addDocument(queryDoc);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private boolean isRelevant(SearchResult document) {
        if (isPseudo)
            return (document.getRank() <= pseudoK);
        else
            return document.isRelevant();
    }

    /**
     * Ensure that a Lucene index exists. If not then create a new index using the
     * in-memory RAMdirectory. This allows for very fast indexing and term retrieval.
     */
    private void checkAndInitializeIndex() {
        if (indexDirectory == null) {
            indexDirectory = new RAMDirectory();

            analyzer = new IQITAnalyzer(Version.LUCENE_7_5_0);

            try {
                IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
                iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

                //IndexWriter writes new index files to the directory
                writer = new IndexWriter(indexDirectory, iwc);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void clearRelevantDocuments() {
        setDocuments(new ArrayList<>());
        if (indexDirectory != null) indexDirectory.close();
        indexDirectory = null;
        analyzer = null;
        writer = null;
    }

    public void setIsPseudo(boolean isPseudo) {
        this.isPseudo = isPseudo;
    }

    public void setPseudoK(Integer pseudoK) {
        this.pseudoK = pseudoK;
    }

    public Integer getPseudoK() {
        return pseudoK;
    }
}
