package main;

import data.Result;

public class ScienceDirectResult extends SearchResult {

    private final data.Result res;
    private boolean abstractFetched = false;

    public ScienceDirectResult(data.Result res) {
        this.res = res;
        setTitle(res.getTitle());
    }

    /**
     * lazy-loaded value, on first call it is fetched via http get
     * the result is cached for future access
     */
    public String getAbstractText() {
        String val = super.getAbstractText();
        if (!abstractFetched) {
            val = this.res.getAbstract();
            val = val == null ? "" : val;
            setAbstractText(val);
            {
                // ugly-ish hack:
                // keywords are fetched as a side effect of abstract fetches
                final StringBuilder kws = new StringBuilder();
                for (String kw : res.getKeywords()) {
                    if (kws.length() > 0) kws.append(", ");
                    kws.append(kw);
                }
                setKeywords(kws.toString());
            }
            abstractFetched = true;
        }
        return val;
    }





}