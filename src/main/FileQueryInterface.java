package main;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileQueryInterface implements SearchResultSource {
    private String fileName;
    public static final Pattern FIELD_PATTERN = Pattern.compile("(\\w+) = \"(.*)\",?");

    public FileQueryInterface(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public ArrayList<SearchResult> getResults(String query) {

        ArrayList<SearchResult> searchResults = null;
        int rank = 0;

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String line = null;

            SearchResult searchResult = null;

            while ((line = bufferedReader.readLine()) != null) {

                // If the line starts with @ its a new reference:
                if (line.startsWith("@")) {
                    // If there's an existing reference add to the current reference being accumulated
                    if (searchResult != null) {
                        searchResults.add(searchResult);
                        searchResult = null;
                    }
                } else {
                    Matcher fieldMatcher = FIELD_PATTERN.matcher(line);
                    if (fieldMatcher.matches()) {
                        if (searchResults == null) searchResults = new ArrayList<>();
                        if (searchResult == null) {
                            rank++;
                            searchResult = new SearchResult();
                            searchResult.setRank(rank);
                        }

                        String field = fieldMatcher.group(1);
                        String value = fieldMatcher.group(2);
                        searchResult.fields.put(field,value);
                    }
                }
            }
            if (searchResult != null) searchResults.add(searchResult);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return searchResults;
    }
}
