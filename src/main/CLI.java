package main;

class CLI {

    public static void main(String[] args) {
        try { doStuff(); } catch (Throwable t) { t.printStackTrace(); }
    }


    private static void doStuff() {
        final String query = "UFO"; //"bigfoot sasquatch UFO Area 51";
        SearchResultSource srs = new ScienceDirectResultSource();
        int limit = 5;
        int count = 0;
        for (SearchResult res : srs.getResults(query)) {
            print("got: " + res);
            print("keywords: " + res.getKeywords());
            print("title: " + res.getTitle());
            print("authors: " + res.getAuthorList());
            ++count;
            if (count <= limit) {
                // just to demonstrate lazy-loading of abstracts ...
                final long start = System.currentTimeMillis();
                print("abstract: " + res.getAbstractText());
                print("abstract fetch time: " + (System.currentTimeMillis() - start) + "ms");
            }
            print("");
//            if (count >= limit) return;
        }
    }

    private static void print(String str) {
        System.out.println(str);
    }

}