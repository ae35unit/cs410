package main;

import java.util.*;

import data.Connection;
import data.Result;

public class ScienceDirectResultSource implements SearchResultSource {

    private final int resultLimit;

    /**
     *
     * @param resultLimit limit result size to <= resultLimit
     */
    ScienceDirectResultSource(int resultLimit) {
        this.resultLimit = resultLimit;
    }

    ScienceDirectResultSource() {
        this(100);
    }


    public List<SearchResult> getResults(String query) {
        ArrayList<SearchResult> ret = new ArrayList<SearchResult>();
        try {
            for (Result r : Connection.search(query, this.resultLimit)) {
                ret.add(new ScienceDirectResult(r));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        prefetchAbstracts(ret);
        return ret;
    }

    private void prefetchAbstracts(List<SearchResult> results) {
        final long started = System.currentTimeMillis();
        final LinkedList<SearchResult> workQueue = new LinkedList<>(results);
        Thread[] threads = new Thread[15];
        // create & start threads
        for (int i = 0 ; i < threads.length ; ++i) {
            final int threadNum = i;
            threads[i] = new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        SearchResult res = null;
                        synchronized (workQueue) {
                            res = workQueue.isEmpty() ? null : workQueue.removeFirst();
                        }
                        if (res == null) {
                            return;
                        }
                        res.getAbstractText();
                    }
                }
            });
            threads[i].start();
        }
        // join all threads
        for (int i = 0 ; i < threads.length ; ++i) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("prefetch took " + (System.currentTimeMillis() - started) + "ms");
    }

}