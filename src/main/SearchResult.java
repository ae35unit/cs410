package main;

import java.util.HashMap;

public class SearchResult {

    HashMap<String,String> fields;
    private Boolean isRelevant = false;
    private int rank;

    public SearchResult() {
        this.fields = new HashMap<>();
    }

    public String getTitle() {
        return getFieldVal("title");
    }

    void setTitle(final String title) {
        setFieldVal("title", title);
    }

    /**
     * @return empty string if no abstract found
     */
    public String getAbstractText() {
        return getFieldVal("abstract");
    }

    void setAbstractText(final String val) {
        setFieldVal("abstract", val);
    }

    public String getAuthorList() {
        return getFieldVal("author");
    }

    public String getKeywords() {
        return getFieldVal("keywords");
    }
    public void setKeywords(final String val) {
        setFieldVal("keywords", val);
    }

    private String getFieldVal(final String key) {
        String val = fields.get(key);
        return val == null ? "" : val;
    }

    private void setFieldVal(final String key, final String val) {
        fields.put(key, val == null ? "" : val);
    }

    public Boolean isRelevant() {
        return isRelevant;
    }

    public void setIsRelevant(Boolean isRelevant) {
        this.isRelevant = isRelevant;
    }

    public String getCombinedFields() {
        return getTitle() + " " + getKeywords() + " " + getAbstractText();
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
