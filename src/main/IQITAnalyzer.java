package main;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.tartarus.snowball.ext.EnglishStemmer;

import java.io.IOException;
import java.io.StringReader;

import java.io.IOException;
import java.io.StringReader;

public class IQITAnalyzer extends Analyzer {


        private org.apache.lucene.util.Version matchVersion;

        public IQITAnalyzer(Version matchVersion) {
            this.matchVersion = matchVersion;
        }

        @Override
        protected TokenStreamComponents createComponents(String fieldName) {
            Tokenizer whitespaceTokenizer = new WhitespaceTokenizer();
            TokenStream lowerCaseFilter = new LowerCaseFilter(whitespaceTokenizer);
            TokenStream stopWordAnalyzer = new StopFilter(lowerCaseFilter, EnglishAnalyzer.ENGLISH_STOP_WORDS_SET);
            //TokenStream snowballFilter = new SnowballFilter(stopWordAnalyzer,"English");
            return new TokenStreamComponents(whitespaceTokenizer,stopWordAnalyzer);
        }


}
