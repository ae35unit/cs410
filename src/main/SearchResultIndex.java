package main;

import org.apache.lucene.document.Document;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.RAMDirectory;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Class manages sets of search results.
 * Lucene is used to store and index results for fast access to documents and term counts later.
 */
public class SearchResultIndex {

    private final RAMDirectory ramDir;

    public SearchResultIndex() {

        ramDir = new RAMDirectory();
    }

    public SearchResultIndex(String query, ArrayList<SearchResult> searchResults) {
        this();
        createResultIndex(query, searchResults);
    }

    /**
     * Creates a new index and adds search results to it. This will result in terms being stemmed, stop words removed,
     * etc.
     *
     * @param results
     */
    public void createResultIndex( String query , ArrayList<SearchResult> results ) {

        Analyzer analyzer = new StandardAnalyzer();

        try
        {
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

            //IndexWriter writes new index files to the directory
            IndexWriter writer = new IndexWriter(ramDir, iwc);

            Document queryDoc = new Document();
            queryDoc.add(new TextField("type", "query", Field.Store.YES));
            writer.addDocument(queryDoc);

            int rank = 0;
            for (SearchResult result : results) {
                Document doc = new Document();
                rank++;
                doc.add(new StringField("type","document",Field.Store.YES));
                doc.add(new StringField("rank",String.valueOf(rank),Field.Store.YES));
                doc.add(new TextField("title", result.getTitle(), Field.Store.YES));
                doc.add(new TextField("keywords", result.getKeywords(), Field.Store.YES));
                doc.add(new TextField("abstract", result.getAbstractText(), Field.Store.YES));
                doc.add(new TextField("combined",result.getCombinedFields(),Field.Store.YES));
                writer.addDocument(doc);
            }

            writer.close();
        }
        catch (IOException e)
        {
            //Any error goes here
            e.printStackTrace();
        }

    }
    public void deleteResultIndex() {
        ramDir.close();
    }
}
