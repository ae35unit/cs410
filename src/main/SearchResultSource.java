package main;

import java.util.List;

public interface SearchResultSource {
    public List<SearchResult> getResults(String query);
}
