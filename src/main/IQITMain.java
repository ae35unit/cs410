package main;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Main class of the interactive query improvement tool
 *
 */
public class IQITMain extends JFrame implements TableModelListener {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField queryTextField;
    private JTable resultsTable;
    private JTextField pseudoK;
    private JTextField pseudoAlpha;
    private JTextField pseudoBeta;
    private JTextField pseudoFeedback;
    private JTextField relevanceFeedback;
    private JTextField fileNameTextField;
    private JButton fileSelectButton;
    private JTextField relevanceAlpha;
    private JTextField relevanceBeta;
    private JButton getDataButton;
    private JComboBox dataSourceComboBox;
    private JTextField topTermCount;
    private ResultsTableModel resultsTableModel;
    private SearchResultSource onlineQueryInterface;

    private JFileChooser fileChooser;
    private boolean isPseudoFeedbackDone;

    public IQITMain() {

        // File chooser is to optionally selecct a bibtex text file previously exported from ScienceDirect
        // Check first to see if we've saved the user's previous file selection in the registry and use that as default
        try {
            if (Preferences.userRoot().nodeExists("iqit")) {
                fileChooser = new JFileChooser(new File(Preferences.userRoot().node("iqit").get("filename", "C:")));
            } else
                fileChooser = new JFileChooser();
        } catch (BackingStoreException e) {
            e.printStackTrace();
        }

        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);

        isPseudoFeedbackDone = false;
        initializeParameters();

        // Listen for the user hitting ENTER in the query dialog and kick off the online search
        queryTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToUserRegistry("query", queryTextField.getText());
                executeQuery(queryTextField.getText());
            }
        });

        // If the user click file selection then pop-up the dialog, update the file name field, and save their
        // selection in the registry
        fileSelectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = fileChooser.showOpenDialog(IQITMain.this );

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    fileNameTextField.setText(file.getAbsolutePath());
                    saveToUserRegistry("filename", file.getAbsolutePath());
                }
            }
        });
        fileNameTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToUserRegistry("filename",fileNameTextField.getText());
            }
        });
        topTermCount.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToUserRegistry("toptermcount",topTermCount.getText());
            }
        });
        pseudoK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToUserRegistry("pseudok",pseudoK.getText());
            }
        });
        dataSourceComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveToUserRegistry("datasource",(String)(dataSourceComboBox.getSelectedItem()));
            }
        });
        getDataButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                executeQuery(queryTextField.getText());
            }
        });
    }

    private void initializeParameters() {
        dataSourceComboBox.setSelectedItem(readFromRegistry("datasource","Online Query"));
        queryTextField.setText(readFromRegistry("query",""));
        fileNameTextField.setText(readFromRegistry("filename",""));
        topTermCount.setText(readFromRegistry("toptermcount","5"));
        pseudoK.setText(readFromRegistry("pseudok","10"));
        pseudoAlpha.setText(readFromRegistry("pseudoa","0.5"));
        pseudoBeta.setText(readFromRegistry("pseudob","0.5"));
        relevanceAlpha.setText(readFromRegistry("relevancea","0.5"));
        relevanceBeta.setText(readFromRegistry("relevanceb","0.5"));
    }

    /**
     * Run the query that the user has typed into the query text box
     * and update the pseudo relevance feedback
     * @param queryText query to execute against online source
     */
    private void executeQuery(String queryText) {
        isPseudoFeedbackDone = false;
        final boolean forceUseOnline = true;
        if (forceUseOnline) {
            onlineQueryInterface = new ScienceDirectResultSource(100);
        }
        else {
            onlineQueryInterface = new FileQueryInterface(fileNameTextField.getText());
        }
        List<SearchResult> searchResults = onlineQueryInterface.getResults(queryText);
        resultsTableModel.updateData(searchResults);
    }

    private void saveToUserRegistry(String key, String value) {
        Preferences.userRoot().node("iqit").put(key,value);
    }
    private String readFromRegistry(String key, String defaultValue) {
        try {
            if (Preferences.userRoot().nodeExists("iqit")) {
                return Preferences.userRoot().node("iqit").get(key,defaultValue);
            }
        } catch (BackingStoreException e) {
            e.printStackTrace();
        }

        return defaultValue;
    }

    /**
     * Static method to actually instantiate an instance of our application class, set the title, icon, and
     * other options, and then show the GUI.
     */
    private static void createAndShowGUI() {

        final IQITMain frame;
        frame = new IQITMain();
        frame.setTitle("Interactive Query Improvement Tool");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        InputStream icon = IQITMain.class.getResourceAsStream("/images/MainIcon.png");
        if (icon != null) {
            try {
                frame.setIconImage(ImageIO.read(icon));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        frame.setVisible(true);

//        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);


    }

    /**
     * A static main method that will process any command-line argument and then create an instance of the GUI
     * class and show it to the user.
     * @param args arguments passed from the command-line
     */
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        try {
            String className = UIManager.getCrossPlatformLookAndFeelClassName(); //"com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"
            // ^^ this threw on my mac when set to nimbus -jackson
            UIManager.setLookAndFeel(className);
        } catch (Exception e) {
            e.printStackTrace();
        }
        UIManager.put("control", new Color(243,243,243));

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });

    }

    /**
     * Sets up the search results table with defaults for column widths etc.
     */
    private void setupTable() {
        resultsTable = new JTable() {
            public String getToolTipText(MouseEvent e) {
                String tip = null;
                java.awt.Point p = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColumnIndex = convertColumnIndexToModel(colIndex);

                if (realColumnIndex > 1 && rowIndex >= 0) {
                    tip = "" + getValueAt(rowIndex, colIndex);
                }
                else { //another column
                    //You can omit this part if you know you don't
                    //have any renderers that supply their own tool
                    //tips.
                    tip = super.getToolTipText(e);
                }
                return "<html><p width=\"500\">"+tip+"</p></html>" ;
            }
        };

        resultsTableModel = new ResultsTableModel();
        resultsTableModel.addTableModelListener(resultsTable);
        resultsTableModel.addTableModelListener(this);


        resultsTable.setAutoCreateRowSorter(true);

        resultsTable.setAutoCreateColumnsFromModel(true);
        resultsTable.setModel(resultsTableModel);


        resultsTable.setFillsViewportHeight(true);
        resultsTable.setBackground(this.getBackground());

        resultsTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        TableColumnModel columnModel = resultsTable.getColumnModel();
        columnModel.getColumn(0).setMaxWidth(40);
        columnModel.getColumn(1).setMaxWidth(60);
        columnModel.getColumn(2).setMaxWidth(500);
        columnModel.getColumn(3).setMaxWidth(500);
        columnModel.getColumn(0).setPreferredWidth(40);
        columnModel.getColumn(1).setPreferredWidth(60);
        columnModel.getColumn(2).setPreferredWidth(300);
        columnModel.getColumn(3).setPreferredWidth(300);

    }

    /**
     * "Manually" creates any components that are configured as Custom Create in the UI forms builder in
     * IntelliJ.
     *
     */
    private void createUIComponents() {
        setupTable();

        dataSourceComboBox = new JComboBox();
        fileNameTextField = new JTextField();
        queryTextField = new JTextField();
        pseudoK = new JTextField();
        pseudoAlpha = new JTextField();
        pseudoBeta = new JTextField();
        relevanceAlpha = new JTextField();
        relevanceBeta = new JTextField();

    }

    /**
     * Method triggered when the results table changes, either because of a new query or because a relevance
     * rating has changed.
     * @param e
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (!isPseudoFeedbackDone) {
            updatePseudoRelevanceFeedback();
        }
        updateRelevanceFeedback();
    }
    /**
     * Causes the pseudo relevance feedback to be recalculated and the GUI updated with the results.
     */
    private void updatePseudoRelevanceFeedback() {
        List<SearchResult> relevanceFeedbackList = resultsTableModel.getSearchResults();
        float alpha = Float.parseFloat(pseudoAlpha.getText());
        float beta = Float.parseFloat(pseudoBeta.getText());
        float gamma = 0.2f;
        String query = queryTextField.getText();
        String resultString = getFeedbackString(query,relevanceFeedbackList,alpha,beta,gamma,true,Integer.parseInt(pseudoK.getText()));

        pseudoFeedback.setText(resultString);
        isPseudoFeedbackDone = true;
    }

    private void updateRelevanceFeedback() {
        List<SearchResult> relevanceFeedbackList = resultsTableModel.getSearchResults();
        float alpha = Float.parseFloat(relevanceAlpha.getText());
        float beta = Float.parseFloat(relevanceBeta.getText());
        float gamma = 0.2f;
        String query = queryTextField.getText();
        String resultString = getFeedbackString(query,relevanceFeedbackList,alpha,beta,gamma,false,0);

        relevanceFeedback.setText(resultString);
    }

    private String getFeedbackString(String query, List<SearchResult> relevanceFeedbackList, float alpha, float beta,
                                     float gamma, boolean isPseudo, int pseudoK ) {

        int topTermCountInt = Integer.parseInt(topTermCount.getText());


        QueryExtender queryExtender = new QueryExtender();
        queryExtender.setQuery(query);
        queryExtender.setAlpha(alpha);
        queryExtender.setBeta(beta);
        queryExtender.setGamma(gamma);
        queryExtender.setTopTermsPerDocument(topTermCountInt);
        queryExtender.setIsPseudo(isPseudo);
        queryExtender.setPseudoK(pseudoK);
        queryExtender.setDocuments(relevanceFeedbackList);

        LinkedHashMap<String,Double> result = queryExtender.getRocchioFeedback();

        String resultString = "";

        int i = 0;
        for (Map.Entry<String,Double> entry : result.entrySet()) {
            resultString = resultString + String.format(" %s (%.2f),",entry.getKey(), entry.getValue());
            i++;
            if (i==topTermCountInt) break;
        }
        return resultString;
    }

}
