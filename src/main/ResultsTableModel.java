package main;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class ResultsTableModel extends AbstractTableModel {
    private List<SearchResult> searchResults;

    private String[] columnList = { "Rank", "Relevant", "Title", "Keywords", "Abstract"};
    public ResultsTableModel() {
        updateData(null);
    }
    @Override
    public int getRowCount() {
        return this.searchResults.size();
    }

    @Override
    public int getColumnCount() {
        return columnList.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnList[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0,columnIndex).getClass();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 1;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        super.setValueAt(aValue, rowIndex, columnIndex);
        searchResults.get(rowIndex).setIsRelevant((Boolean)aValue);
        fireTableCellUpdated(rowIndex,columnIndex);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SearchResult searchResult = searchResults.get(rowIndex);
        switch(columnIndex) {
            case 0: return (rowIndex+1);
            case 1: return searchResult.isRelevant();
            case 2: return searchResult.getTitle();
            case 3: return searchResult.getKeywords();
            case 4: return searchResult.getAbstractText();
            default: return "Error";
        }
    }

    /**
     * Update all of the data in the table and then recalculate feedback.
     *
     * @param searchResults list of references downloaded from the online database
     */
    public void updateData(List<SearchResult> searchResults) {
        boolean resultsBefore = this.searchResults != null && this.searchResults.size() > 0;
        boolean resultsNow = searchResults != null && searchResults.size() > 0;
        if (resultsNow) {
            this.searchResults = searchResults;
        }
        else {
            this.searchResults = new ArrayList<SearchResult>();
        }

        // Fire a table data change event, which in the main GUI program will be detected in order
        // to kick off the recalculation of pseudo feedback.
        if (resultsNow) {
            fireTableDataChanged();
        }

    }

    public List<SearchResult> getTopK(Integer k) {
        return searchResults.subList(0, k);
    }

    public List<SearchResult> getReleventDocs() {
        List<SearchResult> relevantDocs = new ArrayList<>();
        for (SearchResult searchResult : searchResults) {
            if (searchResult.isRelevant())
                relevantDocs.add(searchResult);
        }
        return relevantDocs;
    }

    public List<SearchResult> getSearchResults() {
        return searchResults;
    }
}
