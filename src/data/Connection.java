package data;

import java.io.*;
import java.net.*;
import java.util.stream.*;

import org.json.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;


public class Connection {

  /*
   * API Docs:
   * search: https://dev.elsevier.com/documentation/SCIDIRSearchAPI.wadl
   */

  /**
   *
   * @param query
   * @param resultLimit - return no more than this many results
   * @return
   * @throws Exception
   */
  public static Result[] search(final String query, final int resultLimit) throws Exception {
    final String strResult = fetchJSON(query);
    final JSONObject obj = new JSONObject(strResult);
    JSONObject res = obj.getJSONObject("search-results");
    JSONArray arr = null;
    if (res.has("entry")) {
      arr = res.getJSONArray("entry");
    } else {
      return new Result[0];
    }
    Result[] results;
    if (arr.get(0).toString().indexOf("Result set was empty.") > -1) {
      results = new Result[0];
    } else {
      results = new Result[arr.length()];
      for (int i = 0; i < arr.length(); ++i) {
        results[i] = new Result(arr.getJSONObject(i));
      }
    }
    // truncate result set if we found too many ...
    if (results.length > resultLimit) {
      Result[] replacement = new Result[resultLimit];
      for (int i = 0 ; i < resultLimit ; ++i) {
        replacement[i] = results[i];
      }
      results = replacement;
    }
    return results;
  }

  public static Result[] search(final String query) throws Exception {
    return search(query, 100);
  }



  static final String API_KEY = "5f91cff6494321ef0045338f9794eaf1";
  private static final String SEARCH_URL = "https://api.elsevier.com/content/search/sciencedirect";

  // private static final String 

  private Connection() {}

  private static String fetchJSON(final String query) throws Exception {
    // https://www.mkyong.com/webservices/jax-rs/restfull-java-client-with-java-net-url/
    HttpURLConnection conn = null;
    String result = null;
    String urlStr = SEARCH_URL;
    urlStr += "?apiKey=" + API_KEY;
    urlStr += "&view=COMPLETE";
    urlStr += "&query=" + java.net.URLEncoder.encode(query, "utf8");
    urlStr += "&count=" + 100;
    result = doGet(urlStr);
    JSONUtils.writeCache(result);
    return result;
  }

  static String doGet(final String urlStr) throws Exception {
    final long started = System.currentTimeMillis();
    String result = readCache(urlStr);
    if (result == null) {
      HttpURLConnection conn = null;
      try {
        final URL url = new URL(urlStr);
        conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        if (conn.getResponseCode() != 200) {
          throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }
        final BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        result = br.lines().collect(Collectors.joining());
        // System.out.println(result);
      } finally {
        if (conn != null) {
          conn.disconnect();
        }
      }
      writeCache(urlStr, result);
    }
    print("" + (System.currentTimeMillis() - started) + "ms: " + urlStr);
    return result;
  }


  private static final File CACHE_DIR = new File(new File("."), "scidir-cache");
  static {
    if (!CACHE_DIR.exists()) {
      CACHE_DIR.mkdirs();
    }
  }
  private static File cacheFile(final String url) {
    return new File(CACHE_DIR, DigestUtils.sha1Hex(url).substring(0,10) + ".txt");
  }
  private static String readCache(final String url) {
    try {
      return FileUtils.readFileToString(cacheFile(url));
    } catch (IOException e) {
    }
    return null;
  }
  private static void writeCache(final String url, final String value) {
    try {
      FileUtils.writeStringToFile(cacheFile(url), value);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  private static void print(String s) { System.out.println(s); }



}
