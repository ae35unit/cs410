package data;

import org.json.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class Result {

  private final JSONObject _obj;

  Result(JSONObject obj) {
    this._obj = obj;
  }

  public String getPublicationName() {
    return this._obj.getString("prism:publicationName");
  }

  public String getTitle() {
    return this._obj.getString("dc:title");
  }

  public String getPii() {
    return this._obj.getString("pii");
  }


  private String _abstract;
  private Set<String> _keywords = new TreeSet<String>();

  public Set<String> getKeywords() {
    return new TreeSet(_keywords);
  }

  public String getAbstract() {
    if (_abstract != null) {
      return _abstract;
    }
    String retval = ""; // <-- failed fetch results in returning this
    // print("entering getAbstract() for " + getTitle());
    // https://dev.elsevier.com/documentation/AbstractRetrievalAPI.wadl#d1e894
    String url = "https://api.elsevier.com/content/abstract/pii";
    url += "/" + getPii();
    url += "?apiKey=" + Connection.API_KEY;
    String json = null;
    boolean getSuccess = false;
    try {
      json = Connection.doGet(url);
      getSuccess = true;
    } catch (Exception e) {
      // http get fail
    }
    if (getSuccess) {
      try {
        JSONObject jobj = new JSONObject(json);
        // while we have the data handy, fish any available keywords out of it
        try {
          final JSONArray arr = jobj.getJSONObject("abstracts-retrieval-response").getJSONObject("authkeywords").getJSONArray("author-keyword");
          for (Object o : arr.toList()) {
            HashMap h = (HashMap)o;
            for (Object v : h.values()) {
              String s = v.toString();
              if (s.equals("true")) continue;
              _keywords.add(v.toString());
            }
          }
        } catch (Exception e) {
          // yes, this is a hack
          // happens if keywords are missing
        }

        //      JSONUtils.printKeys(jobj);
        //      JSONUtils.printVals(jobj);
        JSONObject vals = jobj.getJSONObject("abstracts-retrieval-response");
        //      JSONUtils.printKeys(vals);
        //      JSONUtils.printVals(vals);
        JSONObject core = vals.getJSONObject("coredata");
        String val = core.getString("dc:description");
        for (String strip : new String[]{
                "© .* Elsevier B.V.",
                "© .* Elsevier Ltd.",
                "© .* Elsevier ....",
        }) {
          val = val.replaceAll(strip, "");
        }
        retval = val;
      } catch (Exception e) {
        // no description
      }
    }
    _abstract = retval;
    return retval;
  }


  public String toString() {
    return this._obj.toString();
  }

  private static void print(String s) { System.out.println(s); }

}