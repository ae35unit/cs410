package data;

import data.*;

public class CLI {

  private static void print(String str) { System.out.println(str); }

  public static void main(String[] args) {
    try {
      boolean useCache = false;
      Result[] results = Connection.search("thing", 100);
      int limit = 2;
      int count = 0;
      for (Result res : results) {
        print("pub: " + res.getPublicationName());
        print("pii: " + res.getPii() + " title: " + res.getTitle());
        String abs = res.getAbstract();
        print("abstract (" + abs.length() + "): " + res.getAbstract());
        print("\n");
        ++count;
        if (count >= limit) {
          print("that's " + count + ", limit set to " + limit + " so skipping the other " + (results.length - count) + " and exiting now.");
          return;
        }
        //try { Thread.currentThread().sleep(1000); } catch (Throwable t) { }
      }
    } catch (Throwable t) {
      t.printStackTrace(System.err);
      System.exit(1);
    }
  }

}