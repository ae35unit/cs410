package data;

import java.io.*;
import java.util.*;
import java.util.stream.*;
import org.json.*;

class JSONUtils {

  /** cached query file */
  private static final File CACHE = new File("cache.json");

  private static void print(String str) { System.out.println(str); }

  public static void printKeys(final JSONObject obj) {
    print("printKeys >>");
    for (Iterator keys = obj.keys() ; keys.hasNext() ; ) {
      final String key = keys.next().toString();
      print("key: '" + key + "'");
    }
    print("<< printKeys\n");
  }

  public static void printVals(final JSONObject obj) {
    print("printVals >>");
    for (Iterator keys = obj.keys() ; keys.hasNext() ; ) {
      final String key = keys.next().toString();
      final String val = obj.get(key).toString();
      print("key: '" + key + "' val: '" + val + "'");
    }
    print("<< printVals\n");
  }

  /**
  load cached query
   */
  static String readCache(File file) throws Exception {
    StringBuilder sb = new StringBuilder();
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(file));
      for (String line = br.readLine() ; line != null ; line = br.readLine()) {
          sb.append(line).append(System.lineSeparator());
      }
    } finally {
        if (br != null) {
          br.close();
        }
    }
    return sb.toString();
  }
  static String readCache() throws Exception {
    return readCache(CACHE);
  }

  /**
  write cached query
   */
  static void writeCache(String json, File file) throws Exception {
    BufferedWriter writer = null;
    try {
        writer = new BufferedWriter(new FileWriter(file));
        writer.write(json);
    } finally {
      if (writer != null) {
        writer.close();
      }
    }
  }
  static void writeCache(String json) throws Exception {
    writeCache(json, CACHE);
  }


}